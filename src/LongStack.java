import java.util.LinkedList;

public class LongStack {

   private LinkedList<Long> list = new LinkedList<Long>();

   public static void main (String[] argum) {
      // TODO!!! Your tests here!
   }

   LongStack() {
      // TODO!!! Your constructor here!
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack clonedStack = new LongStack();
      clonedStack.list.addAll(list);
      return clonedStack;
   }

   public boolean stEmpty() {
      return list.size() == 0;
   }

   public void push (long a) {
      try {
         list.push(a);
      } catch (RuntimeException e) {
         throw new RuntimeException("Can't push that element");
      }
   }

   public long pop() {
      try {
         return list.pop();
      } catch (RuntimeException e) {
         throw new RuntimeException("List doesn't have element to pop");
      }
   }

   public void op (String s) {
     if (list.size() > 1) {
        Long secondNumber = list.pop();
        Long firstNumber = list.pop();
        switch (s) {
           case "+":
              list.push(firstNumber + secondNumber);
              break;
           case "-":
              list.push(firstNumber - secondNumber);
              break;
           case "*":
              list.push(firstNumber * secondNumber);
              break;
           case "/":
              list.push(firstNumber / secondNumber);
              break;
            default:
                throw new RuntimeException(String.format("Wrong operation: %s", s));
        }
     } else {
        throw new RuntimeException(String.format("Not enough numbers: %s", s));
     }
   }
  
   public long tos() {
      try {
         return list.getFirst();
      } catch (RuntimeException e) {
         throw new RuntimeException("List doesn't have first element");
      }
   }

   @Override
   public boolean equals (Object o) {
      if (!(o instanceof LongStack)) {
         return false;
      }
      LongStack cast = (LongStack) o;
      if (cast.list.size() != list.size()) {
         return false;
      }
      for (int i = 0; i <= cast.list.size() - 1; i++) {
         if (!list.get(i).equals(cast.list.get(i))) {
            return false;
         }
      }
      return true;
   }

   @Override
   public String toString() {
      StringBuilder message = new StringBuilder();
      if (list.isEmpty()) {
         return message.toString();
      }
      for (int i = 0; i < list.size(); i++) {
         message.append(String.valueOf(list.indexOf(i))).append(" ");
      }
      return message.toString();
   }

    public static long interpret (String pol) {
        LongStack longStack = new LongStack();

        checkEmptyExpression(pol);
        checkIllegalArguments(pol);

        String[] parts = pol.split(" ");

        for (String part : parts) {
            part = part.trim();
            if (part.isEmpty()) { continue; }

            try {
                if ("SWAP".equals(part)) {
                    longStack.swap();
                    continue;
                }
                if ("ROT".equals(part)) {
                    longStack.rot();
                    continue;
                }
            }catch (Exception e) {
                throw new RuntimeException(String.format("Can not perform %s in expression: %s", part, pol));
            }

            if (Character.isDigit(part.charAt(0)) || part.length() > 1) {
                longStack.push(Long.valueOf(part));
            } else {
                try {
                    longStack.op(part);
                } catch (Exception e) {
                    throw new RuntimeException(String.format("Can not perform %s in expression: %s", part, pol));
                }
            }
        }

        if (longStack.list.size() > 1) {
            throw new RuntimeException(String.format("Too many numbers in expression: %s", pol));
        }

        return longStack.pop();
    }

    private static void checkIllegalArguments(String pol) {
       // Viide: Aleksei Žavoronkov
        String[] parts = pol.split(" ");

        for (String part : parts) {
            part = part.trim();
            if (part.isEmpty() || "SWAP".equals(part) || "ROT".equals(part)) { continue; }
            if (Character.isAlphabetic(part.charAt(0))) {
                throw new RuntimeException(String.format("Illegal argument %s in expression: %s", part, pol));
            }
            if (!Character.isDigit(part.charAt(0))) {
                if (part.length() > 1 && !Character.isDigit(part.charAt(1))) {
                    if (!"+-*/".contains(part)) {
                        throw new RuntimeException(String.format("Illegal argument %s in expression: %s", part, pol));
                    }
                }
            }
        }
    }

    private static void checkEmptyExpression(String pol) {
        // Viide: Aleksei Žavoronkov
        if (pol == null) {
            throw new RuntimeException("Empty expression");
        }

        String checkPol = pol.replaceAll(" ", "");

        if (checkPol.length() == 0) {
            throw new RuntimeException("Empty expression");
        }

    }

    public void swap() {
        if (list.size() < 2) {
            throw new RuntimeException("Can not perform swap operation, few elements for it!");
        }

        long second = pop();
        long first = pop();
        push(second);
        push(first);
    }

    public void rot() {
        if (list.size() < 3) {
            throw new RuntimeException("Can not perform rot operation, few elements for it!");
        }

        long first = pop();
        long second = pop();
        long third = pop();
        push(second);
        push(first);
        push(third);
    }

}

